import numpy as np
def area_of_circle (radius, print_res = True):
  area = np.pi * radius**2
  if (print_res):
    print("Daljits area is " + str(np.round(area, 1)))
    
  return(area)

area_of_circle(1)

area_of_circle(2, False)

print(area_of_circle)
# Workshop GIT example
# Adding branch